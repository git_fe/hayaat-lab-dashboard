import { Component, ViewContainerRef, OnInit } from '@angular/core';
import { ToastsManager} from "ng2-toastr";
import {CookieService} from "ngx-cookie-service";
declare var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
  // Set toastr container ref configuration for toastr positioning on screen
  constructor(public toastr: ToastsManager, vRef: ViewContainerRef, private cookieService: CookieService) {
    this.toastr.setRootViewContainerRef(vRef);
  }
  ngOnInit() {
    if (this.cookieService.get('role') === '2') {
      this.navHide();
    }
  }
  navHide() {
    setTimeout(() => {
      $('ul.navigation li:nth-child(5)').css('display', 'none', 'important');
      $('ul.navigation li:nth-child(6)').css('display', 'none', 'important');
      $('ul.navigation li:nth-child(7)').css('display', 'none', 'important');
    }, 1000);
  }
}
