import swal from 'sweetalert2';

// Simple Alert
export function basicAlert() {
  swal("Here's a message!").catch(swal.noop); // Use ".catch(swal.noop)" for overlay click close error.
}

// Alert with Title
export function withTitle() {
  swal("Here's a message!", "It's pretty, isn't it?");
}

//  HTML Alert
//  HTML Alert
export function htmlAlert() {
  swal({
    title: 'HTML <small>Title</small>!',
    text: 'A custom <span style="color:#F6BB42">html<span> message.',
    html: '<span></span>'
  });
}

// Question Type Alert
export function typeQuestion() {
  swal("Question", "Are You Sure?", "question");
}

// Success Type Alert
export function typeSuccess() {
  swal("Good job!", "You clicked the button!", "success");
}

// Info Type Alert
export function typeInfo() {
  swal("Info!", "You clicked the button!", "info");
}

// Warning Type Alert
export function typeWarning() {
  swal("Warning!", "You want to Delete the Branch?", "warning");
}

// Error Type Alert
export function typeError() {
  swal("Error!", "You clicked the button!", "error");
}

// Custom Icon
export function customIcon() {
  swal({ title: "Sweet!", text: "Here's a custom image.", imageUrl: "./assets/img/portrait/avatars/avatar-08.png" });
}

// Auto close timer
export function autoClose() {
  swal({ title: "Auto close alert!", text: "I will close in 2 seconds.", timer: 2000, showConfirmButton: false });
}

// Allow Outside Click
export function outsideClick() {
  swal({
    title: 'Click outside to close!',
    text: 'This is a cool message!',
    allowOutsideClick: true
  });
}

