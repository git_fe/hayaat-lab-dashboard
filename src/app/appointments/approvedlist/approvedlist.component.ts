import { Component, OnInit, ViewChild } from '@angular/core';
// import { DatatableComponent } from '@swimlane/ngx-datatable/release';
import { AllOrdersService } from "../services/all-orders.service";
import { environment } from '../../../environments/environment';


declare var require: any;
const data: any = require('../../appointments/data.json');

@Component({
  selector: 'app-approvedlist',
  templateUrl: './approvedlist.component.html',
  styleUrls: ['./approvedlist.component.scss']
})
export class ApprovedlistComponent implements OnInit {
  imagePath = environment.baseUrl;
  rows = [];
  temp = [];
  // @ViewChild(DatatableComponent) table: DatatableComponent;
  allOrdersData= [];
  pagination = [];
  orderId;
  noOrdersData = false;
  noAppt = false
  count;
  page = 1;
  constructor(private allData: AllOrdersService) {
  }
  ngOnInit() {
    this.getData();
  }
  getOrderId(id) {
    this.orderId = id;
  }
  delOrderCall(event) {
    if (event) {
      this.getData();
    }
  }
  getData() {
    this.allOrdersData = [];
    const url = environment.apiUrl + '/orders';
    this.allData.allOrders(url).subscribe(
      (orderData: any[]) => {
        if (orderData['success'] === true) {
          if (orderData['data']['orders'].length !== 0) {
            this.allOrdersData = orderData['data']['orders'];
            this.pagination = orderData['data']['pagination'];
            this.count = orderData['data']['pagination']['last_page'] + '0';
            this.noOrdersData = false;
          } else {
            this.noOrdersData = true;
          }
        }
      }
    );
  }
  pageChanged(event) {
    this.allOrdersData = [];
    const nextUrl = environment.apiUrl + '/orders' + '?page=' + event;
    this.allData.allOrders(nextUrl).subscribe(
      (data) => {
        if (data['success'] === true) {
          this.allOrdersData = data['data']['orders'];
          this.pagination = data['data']['pagination'];
          this.count = data['data']['pagination']['last_page'] + '0';
          this.noAppt = false;
        }
      });
  }
}
