export var settings = {
  columns: {
    id: {
      title: 'ID',
      filter: false,
    },
    name: {
      title: 'Item Name',
      filter: false,
    },
    quantity: {
      title: 'Quantity',
      filter: false,
    },
    unitprice: {
      title: 'Unit Price',
      filter: false,
    },
    price: {
      title: 'Price',
      filter: false,
    },
  },
};

export var data = [
  {id: 1, name: 'item one', quantity : 20 , unitprice : 100 , price: 500 },
  {id: 2, name: 'item one', quantity : 20 , unitprice : 100 , price: 500 }
];
