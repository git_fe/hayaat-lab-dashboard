import { Component, OnInit, TemplateRef, ElementRef, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import {BsModalRef } from "ngx-bootstrap";
import { PharmacyBranchesService } from "../services/pharmacy-branches.service";
import { ToastsManager } from "ng2-toastr";


@Component({
  selector: 'app-update-pharmacy',
  templateUrl: './update-pharmacy.component.html',
  styleUrls: ['./update-pharmacy.component.scss']
})
export class UpdatePharmacyComponent implements OnInit {
  // variables, arrays for component
  pharmacyData = [];
  adminData= [];
  modalRef: BsModalRef;
  // custructor
  constructor(private modalService: BsModalService, private branchData: PharmacyBranchesService, public toastr: ToastsManager, public element: ElementRef) { }
  // modal popup code
  openModal(template: TemplateRef<any>) {
    this.getBranchData();
    setTimeout(() => {
      this.modalRef = this.modalService.show(template) }, 1300);
  }
  // toastr functions
  typeSuccess() {
    this.toastr.success( this.pharmacyData['message'], 'Success!');
  }
  typeError() {
    this.toastr.error(this.pharmacyData['message'], 'Oopss!!!');
  }
  // ng of this class
  ngOnInit() {
  }
  // get branch data for pre-filled fields
  getBranchData() {
    this.branchData.getBranchDetail().subscribe(
      (data: any[]) => {
        this.pharmacyData = data['data'];
        this.adminData = data['data']['branchAdmin'];
        console.log(data);
      }
    );
  }
  // function for updating the branch data
  updatePharmacyBranch(form) {
    const params = 'name=' + form.name + '&email=' + form.email + '&code=' + form.code + '&phone=' + form.phone
      + '&address=' + form.address + '&fax=' + form.fax + '&manager_name=' + form.admin_name
      + '&manager_phone=' + form.admin_phone + '&_method=put';

    this.branchData.updateBranch(params).subscribe(
      (data: any[]) => {

        this.pharmacyData = data;
        if (this.pharmacyData['success'] === true) {
          this.typeSuccess();
        } else if (this.pharmacyData['success'] === false) {
          this.typeError();
        }
      }
    );
  }
}
