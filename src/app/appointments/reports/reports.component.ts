import {Component, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {ToastsManager} from 'ng2-toastr';
import {AllOrdersService} from '../services/all-orders.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  form: FormGroup;
  myFiles: string [] = [];
  files = [];
  ordersNo = [];
  thumb_url = [];
  loaded = false;
  sendingbtnRequest = false;
  sendingRequest = false;
  form1: FormGroup;
  orderNo = null;
  customerDetail = [];
  productDetail = [];
  filesToUpload: Array<File> = [];
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  @ViewChild('instance') instance: NgbTypeahead;
  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200).distinctUntilChanged()
      .merge(this.focus$)
      .merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term => (term === '' ? this.ordersNo : this.ordersNo.filter(v => v.order_number.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10));
  formatter = (x: {order_number: string}) => x.order_number;
  constructor(private orderService: AllOrdersService, private fb: FormBuilder, private toastr: ToastsManager) {
  }

  ngOnInit() {
    this.createForm();
    this.getOrderNumbers();
  }

  createForm() {
    this.form = this.fb.group({
      order_number: ['', Validators.required],
      image: ['', Validators],
      avatar: null
    });
  }

  getOrderNumbers() {
    this.orderService.allOrdersNo().subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          this.ordersNo = data['data'];
          this.typeSuccess(data['message']);
        }
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          this.typeError(err.error.message);
        }
      }
    );
  }

  selectedId(row, index) {
    this.orderNo = this.ordersNo.find(x => x.id === row.item.id);
    this.orderNo = this.orderNo['id'];
  }

  getDetail(formData, event: Event) {
      event.preventDefault();
      this.sendingRequest = true;
    if (this.orderNo == null) {
      this.orderNo = this.ordersNo.find(x => x.id === formData.item.id);
      this.orderNo = this.orderNo['id'];
    } else {
      this.orderNo = this.orderNo ;
    }
    this.orderService.orderDetail(this.orderNo).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          this.customerDetail = data['data'];
          this.productDetail = data['data']['orderItems'];
          this.typeSuccess(data['message']);
          this.loaded = true;
          this.sendingRequest = false;
        }
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          this.typeError(err.error.message);
          this.loaded = false;
          this.sendingRequest = false;
        }
      }
    );
  }


  onFileChange(e) {
    if (e.target.files.length > 0) {
      if (e.target.files.length > 5 || this.myFiles.length > 4) {
        this.typeError('Image count exceeded')
      } else {
        this.files = [];
        this.filesToUpload = <Array<File>>e.target.files;
        for (let i = 0; i < e.target.files.length; i++) {
          if (e.target.files[i].type === 'application/pdf') {
            this.myFiles.push(e.target.files[i])
            // if (e.target.files.length > 0) {
            //   this.readUrl(e);
            // }
          } else {
            this.typeError('Please Upload PDF File Only');
          }
        }
      }
    }
  }
  readUrl(event: any) {
    this.thumb_url = [];
    for (let x = 0; x <= this.filesToUpload.length; x++) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.thumb_url.push(e.target.result);
      };
      reader.readAsDataURL(event.target.files[x]);
    }
  }
  delUploadedImage(imgIndex) {
    this.files.splice(imgIndex, 1);
    this.myFiles.splice(imgIndex, 1);
    if (this.myFiles.length === 0) {
      this.form.get('image').setValue('');
      this.form.get('avatar').setValue(null);
    }
  }

  sendOrder(formData) {
    this.sendingbtnRequest = true;
    const frmDatas = new FormData();
    for (let i = 0; i < this.myFiles.length; i++) {
      frmDatas.append('report[]', this.myFiles[i]);
    }
    frmDatas.append('order_id', this.orderNo);
    this.orderService.saveFiles(frmDatas).subscribe(
      (orderResponseData: any[]) => {
        if (orderResponseData['success'] === true) {
          this.typeSuccess(orderResponseData['message']);
          this.sendingbtnRequest = false;
         this.loaded = false;
         this.form.get('image').setValue('');
         this.form.get('avatar').setValue('');
        }
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          this.sendingbtnRequest = false;
          this.typeError(err.error.message);
        }
      }
    );
  }
  typeSuccess(message) {
    this.toastr.success( message, 'Success!');
  }
  typeError(message) {
    this.toastr.error(message, 'Oopss!!!');
  }

}
