import {Component, OnInit, ViewEncapsulation, TemplateRef, Input, Output, EventEmitter} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { InvoicesService } from "../services/invoices.service";
import { environment } from '../../../environments/environment';
import {HttpErrorResponse} from '@angular/common/http';
import {ToastsManager} from 'ng2-toastr';
// import {OrderComponent} from '../order/order.component';
// import {CompletedOrderComponent} from '../completed-order/completed-order.component';
import {CookieService} from "ngx-cookie-service";



@Component({
  selector: 'app-invoicedetail',
  templateUrl: './invoicedetail.component.html',
  styleUrls: ['./invoicedetail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoicedetailComponent implements OnInit {
  @Input() orderDetails;
  @Input() invoiceId;
  @Input() showBtnComplete;
  @Output() childEvent = new EventEmitter();
  image_src = '';
  image_small = '';
  pathTwo = false;
  orderDetail = [];
  moveLoading = false;
  pathOne = false;
  pathThree = false;
  pathFour = false;
  imagesNull = true;
  total_img_count;
  current_img = '1';
  imagePath = environment.baseUrl;
  customerDetail = [];
  productDetail = [];

  thumbnails = [];
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService, private invoiceService: InvoicesService ,  private toastr: ToastsManager) { }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  ngOnInit() {
    if (this.orderDetails !== '' || this.orderDetails !== undefined) {
      this.orderDetails = false;
    }
    this.getPath();
    this.getPath2();
    this.getPath3();
    this.getPath4();

  }
  completedInoviceDetail() {
    console.log('1');
    if (this.invoiceId !== '' || this.invoiceId !== undefined) {
      this.invoiceService.completeOrderIndex = this.invoiceId;
    }
    this.invoiceService.completeOrderInvoiceData().subscribe(
      (data: any[]) => {
        this.customerDetail = data['data'];
        this.productDetail = data['data']['invoiceItems'];
        if (data['data']['medias'].length !== 0) {
          this.thumbnails = data['data']['medias'];
          this.image_src = environment.baseUrl + '/' + data['data']['medias'][0]['image'];
          this.image_small = environment.baseUrl + '/' + data['data']['medias'][0]['image'];
          this.imagesNull = false;
          this.getCount();
        } else {
          this.image_small = '';
          this.image_src = '';
        }
      }
    );
  }
  ordersInoviceDetail() {
    console.log('2');

    if (this.invoiceId !== '' || this.invoiceId !== undefined) {
      this.invoiceService.orderHistoryIndex = this.invoiceId;
    }
    this.invoiceService.orderInvoiceData().subscribe(
      (data: any[]) => {
        this.customerDetail = data['data'];
        this.productDetail = data['data']['invoiceItems'];
        if (data['data']['medias'].length !== 0) {
          this.thumbnails = data['data']['medias'];
          this.image_src = environment.baseUrl + '/' + data['data']['medias'][0]['image'];
          this.image_small = environment.baseUrl + '/' + data['data']['medias'][0]['image'];
          this.imagesNull = false;
          this.getCount();
        } else {
          this.image_small = '';
          this.image_src = '';
        }
      }
    );
  }
  customerInoviceDetail() {
    console.log('3');

    this.invoiceService.customerInvoiceData().subscribe(
      (data: any[]) => {
         this.customerDetail = data['data'];
         this.productDetail = data['data']['invoiceItems'];
        if (data['data']['medias'].length !== 0) {
          this.thumbnails = data['data']['medias'];
          this.image_src = environment.baseUrl + '/' + data['data']['medias'][0]['image'];
          this.image_small = environment.baseUrl + '/' + data['data']['medias'][0]['image'];
          this.imagesNull = false;
          this.getCount();
        } else {
          this.image_small = '';
          this.image_src = '';
        }
      }
    );
  }
  invoiceDetailData() {
    console.log('4');

    this.invoiceService.invoiceDetail().subscribe(
      (data: any[]) => {
        this.customerDetail = data['data'];
        this.productDetail = data['data']['invoiceItems'];
        if (data['data']['medias'].length !== 0) {
          this.thumbnails = data['data']['medias'];
          this.image_src = environment.baseUrl + '/' + data['data']['medias'][0]['image'];
          this.image_small = environment.baseUrl + '/' + data['data']['medias'][0]['image'];
          this.imagesNull = false;
          this.getCount();
        } else {
          this.image_small = '';
          this.image_src = '';
        }
      }
    );
  }
  // invoice buttons for different paths
  getPath() {
    if (location.pathname === '/datatables/invoice') {
      this.pathOne = true;
    }
  }
  getPath2() {
    if (location.pathname === '/full-layout') {
      this.pathTwo = true;
    }
  }
  getPath3() {
    if (location.pathname === '/datatables/orders') {
      this.pathThree = true;
    }
  }
  getPath4() {
    if (location.pathname === '/datatables/customerdetail') {
      this.pathFour = true;
    }
  }
  // images binding for zoomer
  getImgSrc(src, i) {
    this.image_src = environment.baseUrl + '/' + src;
    this.image_small = environment.baseUrl + '/' + src;
    this.current_img = i + 1;
  }
  getCount() {
    this.total_img_count = this.thumbnails.length;
  }
  moveToComplete(orderId) {
    const confirmBox = confirm('Are you sure?');
    if (confirmBox) {
      this.moveLoading = true;
      const params = 'status=4&_method=put';
      this.invoiceService.updateOrder(orderId, params).subscribe(
        (data: any[]) => {
          if (data['success'] === true) {
            this.moveLoading = false;
            this.modalRef.hide();
            this.childEvent.next('true');
            this.typeSuccess(data['message']);
          }
        },
        (err) => {
          this.moveLoading = false;
          if (err instanceof HttpErrorResponse) {
            this.typeError(err.error.message);
          }
        }
      );
    }
  }
  typeSuccess(message) {
    this.toastr.success( message, 'Success!');
  }
  typeError(message) {
    this.toastr.error(message, 'Oopss!!!');
  }
}
