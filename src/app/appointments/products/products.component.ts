import {Component, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {ToastsManager} from 'ng2-toastr';
import {ProductsService} from '../services/products.service';
// import { DatatableComponent } from '../../../../node_modules/@swimlane/ngx-datatable/src/components/datatable.component';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsComponent implements OnInit {
  model: any;
  temp = [];
  rows = [];
  catId = null;
  proId = null;
  sPrice = null;
  allProducts = [];
  noProductFound = false;
  productName = null;
  form: FormGroup;
  imgLoader = false;
  categories = [];
  products = [];
  categoryID = null;
  productId = null;
  states = [];
  totalRows: any = [];
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  @ViewChild('instance') instance: NgbTypeahead;
  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200).distinctUntilChanged()
      .merge(this.focus$)
      .merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term => (term === '' ? this.states : this.states.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10));
  formatter = (x: {name: string}) => x.name;
  search1 = (text$: Observable<string>) =>
    text$
      .debounceTime(200).distinctUntilChanged()
      .merge(this.focus$)
      .merge(this.click$.filter(() => !this.instance.isPopupOpen()))
      .map(term1 => (term1 === '' ? this.products : this.products.filter(v => v.name.toLowerCase().indexOf(term1.toLowerCase()) > -1)).slice(0, 10));
      formatter1 = (x: {name: string}) => x.name;
  // @ViewChild('table') table: DatatableComponent;
  selectedName(id, index) {
    this.productId = null;
    this.productId = this.products.find(x => x.id === id.item.id);
    this.productId = this.productId['id'];
  }
  selectedId(id, index) {
    this.categoryID = this.states.find(x => x.id === id.item.id);
    this.categoryID = this.categoryID['id'];
  }
  constructor(private productsService: ProductsService, private fb: FormBuilder, private toastr: ToastsManager) {
    setTimeout(() => { this.loadingIndicator = false; }, 1500);
    this.temp = this.rows;
    this.totalRows = this.rows;
  }
  ngOnInit() {
    this.createForm();
    this.getCategories();
    this.getProducts();
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      price: ['', Validators.required]
    });
  }

  saveProduct(formData, event: Event) {
    event.preventDefault();
    if (this.productId == null) {
      this.productName = formData.name;
    } else {
      this.productName = null;
    }
    const params = 'product_id=' + this.productId + '&name=' + this.productName + '&category_id=' + this.categoryID + '&unit_price=' + formData.price;
    this.productsService.saveProduct(params).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          this.getProducts();
          this.form.reset();
          this.typeSuccess(data['message']);
        }
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          this.typeError(err.error.message);
        }
      }
    );
  }

  getCategories(){
    this.productsService.getCategories().subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          this.categories = data['data'];
          this.states = data['data'];
          this.typeSuccess(data['message']);
        }
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          this.typeError(err.error.message);
        }
      }
    );
  }
  getProducts(){
    this.imgLoader = true;
    this.productsService.getProducts().subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          this.products = data['data']['products'];
          this.rows = this.products;
          this.allProducts = this.products;
          this.imgLoader = false;
          this.typeSuccess(data['message']);
          this.noProductFound = false;
        }
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          this.imgLoader = true;
          this.noProductFound = true;
          this.typeError(err.error.message);
        }
      }
    );
  }
  typeSuccess(message) {
    this.toastr.success( message, 'Success!');
  }
  typeError(message) {
    this.toastr.error(message, 'Oopss!!!');
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    if(val !== '') {
      // filter our data
      const temp = this.rows.filter(function(d) {
        return d.name.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      // this.table.offset = 0;
    } else {
      this.rows = [...this.allProducts];
    }
  }

  deleteProduct(productID, row, event: Event) {
    event.preventDefault();
    const conf = confirm('Are you sure?');
      if ( conf === true) {
          this.productsService.deleteProducts(productID).subscribe(
            (data: any[]) => {
              if (data['success'] === true) {
                this.rows.splice(row.index, 1);
                this.rows = [...this.rows];
                this.typeSuccess(data['message']);
              }
            },
            (err) => {
              if (err instanceof HttpErrorResponse) {
                this.typeError(err.error.message);
              }
            }
          );
        }
  }

}
