import { Component, OnInit } from '@angular/core';
import { HomeDataService } from "../services/home-data.service";
import {InvoicesService } from "../services/invoices.service";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-cancel-order',
  templateUrl: './cancel-order.component.html',
  styleUrls: ['./cancel-order.component.scss']
})
export class CancelOrderComponent implements OnInit {
  status = 3;
  public branches: any = [];
  cancelOrderData = [];
  noCancelData = false;
  branchAdmin = false;
  imgLoader = true;
  constructor(private completeOrder: HomeDataService, private invoiceService: InvoicesService, private cookieService: CookieService) {
    // this.completeOrder.listen().subscribe((m: any) => {
    //   if (m == 'cancel') {
    //     this.getData();
    //   }
    // });
  }

  ngOnInit() {
    this.getData();
    if (this.cookieService.get('role') === '2') {
      this.branchAdmin = true;
    }
  }
  getData() {
    this.completeOrder.homeData().subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data']['canceled_orders'].length !== 0) {
            this.cancelOrderData = data['data']['canceled_orders'];
            this.noCancelData = false;
            this.imgLoader = false;
          } else {
            this.noCancelData = true;
            this.imgLoader = false;
          }
          this.branches = data['data']['branches'];
        }
      }
    );
  }
  cancelOrdersOfBranch(branchId) {
    this.imgLoader = true;
    this.noCancelData = false;
    this.completeOrder.branchOrders(branchId, this.status).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data'].length !== 0) {
            this.cancelOrderData = data['data'];
            this.noCancelData = false;
            this.imgLoader = false;
          } else {
            this.noCancelData = true;
            this.imgLoader = false;
          }
        }
      }
    );
  }
}
