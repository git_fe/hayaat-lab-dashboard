import { Component, OnInit } from '@angular/core';
import { HomeDataService } from "../services/home-data.service";
import {InvoicesService } from "../services/invoices.service";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-completed-order',
  templateUrl: './completed-order.component.html',
  styleUrls: ['./completed-order.component.scss']
})
export class CompletedOrderComponent implements OnInit {
  private status = 2;
  public completeOrdersData: any = [];
  public branches: any = [];
  branchAdmin = false;
  noCompleteData = false;
  imgLoader = true;
  constructor(private completeOrder: HomeDataService, private invoiceService: InvoicesService, private cookieService: CookieService) {
    // this.completeOrder.listen().subscribe((m: any) => {
    //   if (m == 'complete') {
    //     this.getData();
    //   }
    // });
  }

  ngOnInit() {
    this.getData();
    if (this.cookieService.get('role') === '2') {
      this.branchAdmin = true;
    }
  }
  getOrderId(id) {
      this.invoiceService.saveCompleteOrderIndex(id)
  }
  getData() {
    this.completeOrder.homeData().subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data']['complete_orders'].length !== 0) {
            this.completeOrdersData = data['data']['complete_orders'];
            this.noCompleteData = false;
            this.imgLoader = false;
          } else {
            this.noCompleteData = true;
            this.imgLoader = false;
          }
          this.branches = data['data']['branches'];
        }
      }
    );
  }
  completeOrdersOfBranch(branchId) {
    this.imgLoader = true;
    this.noCompleteData = false;
    this.completeOrder.branchOrders(branchId, this.status).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data'].length !== 0) {
            this.completeOrdersData = data['data'];
            this.noCompleteData = false;
            this.imgLoader = false;
          } else {
            this.noCompleteData = true;
            this.imgLoader = false;
          }
        }
      }
    );
  }
}
