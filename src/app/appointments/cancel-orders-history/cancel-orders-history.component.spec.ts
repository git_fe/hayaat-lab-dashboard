import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelOrdersHistoryComponent } from './cancel-orders-history.component';

describe('CancelOrdersHistoryComponent', () => {
  let component: CancelOrdersHistoryComponent;
  let fixture: ComponentFixture<CancelOrdersHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelOrdersHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelOrdersHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
