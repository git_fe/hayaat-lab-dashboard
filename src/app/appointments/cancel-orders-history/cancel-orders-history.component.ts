import { Component, OnInit } from '@angular/core';
import {AllOrdersService} from '../services/all-orders.service';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-cancel-orders-history',
  templateUrl: './cancel-orders-history.component.html',
  styleUrls: ['./cancel-orders-history.component.scss']
})
export class CancelOrdersHistoryComponent implements OnInit {

  canceledOrdersData = [];
  orderId;
  noOrdersData = false;
  imagePath = environment.baseUrl;
  pagination = [];
  count;
  page = 1;
  imgLoader = true;

  constructor(private allOrdersService: AllOrdersService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
  const url = environment.apiUrl + '/orders?cancelled=6';
  this.allOrdersService.ordersCancel(url).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data']['orders'].length !== 0) {
            this.canceledOrdersData = data['data']['orders'];
            this.pagination = data['data']['pagination'];
            this.count = data['data']['pagination']['last_page'] + '0';
            this.noOrdersData = false;
            this.imgLoader = false;
          } else {
            this.noOrdersData = true;
            this.imgLoader = true;

          }
        }
      }
    );
  }
  pageChanged(event) {
    this.canceledOrdersData = [];
    const nextUrl = environment.apiUrl + '/orders?cancelled=6' + '&page=' + event;
    this.allOrdersService.ordersCancel(nextUrl).subscribe(
      (data) => {
        if (data['success'] === true) {
          this.canceledOrdersData = data['data']['orders'];
          this.pagination = data['data']['pagination'];
          this.count = data['data']['pagination']['last_page'] + '0';
          this.noOrdersData = false;
        }
      });
  }
}
