import { Component, OnInit, TemplateRef } from '@angular/core';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { OffersService } from "../services/offers.service";
import {CookieService} from "ngx-cookie-service";
import {ToastsManager} from "ng2-toastr";
import {HttpErrorResponse} from "@angular/common/http";

const now = new Date();

@Component({
  selector: 'app-create-offer',
  templateUrl: './create-offer.component.html',
  styleUrls: ['./create-offer.component.scss']
})
export class CreateOfferComponent implements OnInit {

// Variable declaration
  d: any;
  model: NgbDateStruct;
  popupModel;
  popupModel2;
  date: {year: number, month: number};
  pharmacyAdmin = false;

  configModal;    // Global configuration of datepickers

  branches = [];
  // model popup code
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService,
              private offerService: OffersService,
              private cookieService: CookieService,
  private toastr: ToastsManager) { }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  // Selects today's date
  selectToday() {
    this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
  }

  ngOnInit() {
    this.selectToday();
    this.branchForOffer();
    if (this.cookieService.get('role') === '2') {
      this.pharmacyAdmin = true;
    }
  }
  branchForOffer() {
    this.offerService.branches().subscribe(
      (data: any[]) => {
        this.branches = data['data']['branches'];
        console.log(data);
      }
    );
  }
  saveOffer(form, unit, branch) {
    const startDate = JSON.stringify(form.startDate);
    const endDate = JSON.stringify(form.endDate);
    const params = 'branch=' + branch + '&name=' + form.offerName + '&value=' + form.percentage + '&type=' + unit + '&start=' + startDate
      + '&end=' + endDate;
    // const formData = JSON.stringify(form.EndDate);
    this.offerService.saveOffer(params).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          this.modalRef.hide();
          this.typeSuccess(data['message']);
        }
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          this.typeError(err.error.message);
        }
      }
    );
  }
  typeSuccess(message) {
    this.toastr.success( message, 'Success!');
  }
  typeError(message) {
    this.toastr.error(message['message'], 'Oopss!!!');
  }
}
