import * as shape from 'd3-shape';
// Bar Chart

export let barChartView: any[] = [550, 400];

// options
export let barChartShowXAxis = true;
export let barChartShowYAxis = true;
export let barChartGradient = false;
export let barChartShowLegend = false;
export let barChartShowXAxisLabel = true;
export let barChartXAxisLabel = 'Country';
export let barChartShowYAxisLabel = true;
export let barChartYAxisLabel = 'Population';

export let barChartColorScheme = {
  domain: ['#009DA0', '#FF8D60', '#FF586B', '#AAAAAA']
};
