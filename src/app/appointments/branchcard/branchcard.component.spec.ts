import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchcardComponent } from './branchcard.component';

describe('BranchcardComponent', () => {
  let component: BranchcardComponent;
  let fixture: ComponentFixture<BranchcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
