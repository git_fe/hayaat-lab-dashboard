import { Component, OnInit, ElementRef } from '@angular/core';
import { PharmacyBranchesService } from "../services/pharmacy-branches.service";
import * as alertFunctions from '../../appointments/alerts';
import swal from 'sweetalert2';

@Component({
  selector: 'app-branchcard',
  templateUrl: './branchcard.component.html',
  styleUrls: ['./branchcard.component.scss']
})
export class BranchcardComponent implements OnInit {
  private cardId;
  noBranchFound = false;
  branchesData = [];

  constructor(private branches: PharmacyBranchesService, myElement: ElementRef) {
  }


  ngOnInit() {
    this.getData();
  }

  getData(): void {
    this.branches.allBranches().subscribe(
      (data: any[]) => {
       if (data['success'] === true) {
         if (data['data']['branches'].length !== 0) {
           this.branchesData = data['data']['branches'];
           this.noBranchFound = false;
         } else {
           this.noBranchFound = true;
         }
       }
      }
    );
  }

  getBranchId(branchId, i) {
    this.branches.saveBranchId(branchId);
    this.cardId = i;

  }

  deletePharmacyBranch() {
    const conf = confirm('Are you sure?')
    for (let a = 0; a < this.branchesData.length; a++) {
      if (a === this.cardId) {
        if ( conf === true) {
          this.branchesData.splice(a, 1);
        }
      }
    }
  }
}

