import { Component, OnInit, ViewEncapsulation, TemplateRef, EventEmitter, Input, ViewChild, Output } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as tableData from '../../appointments/smart-table-data';
import { LocalDataSource } from 'ng2-smart-table';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { InvoicesService} from "../services/invoices.service";
import { Observable } from "rxjs/Observable";
import {ToastsManager} from "ng2-toastr";
import {HttpErrorResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { environment } from '../../../environments/environment';
import {Router} from "@angular/router";
import {HomeDataService} from "../services/home-data.service";

@Component({
  selector: 'app-generateinvoice',
  templateUrl: './generateinvoice.component.html',
  styleUrls: ['./generateinvoice.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GenerateinvoiceComponent implements OnInit {

  public model: any;
  public invoiceForm: FormGroup;
  @Input() orderIdInv;
  @Input() orderDetails;
  @Input() flagPending;
  @Input() pendingOrderComponent;
  @Input() completeOrderComponent;
  @Input() cancelOrderComponent;
  @Input() cancelOrderComponentPending;
  @Output() childEvent = new EventEmitter();
  @Output() childEventPending = new EventEmitter();
  total_img_count;
  current_img = '1';
  orderExceed = false;
  image_src = '';
  image_small = '';
  statuses = [];
  objDate;
  products = [];
  unit_prices = [];
  invoiceDetail = [];
  prices = [];
  quantities = [];
  discount = [];
  defaultPromo = [];
  total_price: any = 0;
  subTotal: any = 0;
  percentages: any = 0;
  show = false;
  promocodes: any = [];
  offers: any = [];
  noPromoCode = false;
  noOffer = false;
  thumbnails = [];
  modalRef: BsModalRef;
  source: LocalDataSource;
  imagePath = environment.baseUrl;
  imagesNull = true;
  state = [];
  hayaatDiscount: any = 0;
  loginGif = false;
  loginGifSave = false;
  loginGifImageNot = false;
  loginGifOrderNot = false;
  doctorPrescription = '';
  doctorDetails = [];
  imgLoader = true;
  productArray = [];
  constructor(
    private modalService: BsModalService,
    private _fb: FormBuilder,
    public invoiceService: InvoicesService,
    private router: Router,
    private toastr: ToastsManager,
    private homeService: HomeDataService) {
    this.source = new LocalDataSource(tableData.data); // create the source
  }


  settings = tableData.settings;


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  ngOnInit() {
    this.objDate = Date.now();
    this.invoiceForm = this._fb.group({
      itemRows: this._fb.array([this.initItemRows()])
    });
    // this.getProducts();
  }
  getImgSrc(src , i) {
    this.image_src = environment.baseUrl + '/' + src;
    this.image_small = environment.baseUrl + '/' + src;
    this.current_img = i + 1;
  }
  getCount() {
    this.total_img_count = this.thumbnails.length;
  }
  initItemRows() {
    return this._fb.group({
      item1: [''],
      item2: [''],
      item3: [''],
      item4: ['']
    });
  }

  addNewRow() {
    const control = <FormArray>this.invoiceForm.controls['itemRows'];
    control.push(this.initItemRows());
  }

  deleteRow(index: number) {
    const control = <FormArray>this.invoiceForm.controls['itemRows'];
    control.removeAt(index);
    this.prices.splice(index, 1);
    this.quantities.splice(index, 1);
    this.unit_prices.splice(index, 1);
    this.calPrice();
  }

  save(form) {
    if (confirm('Confirm Order')) {
      this.loginGifSave = true;
      const array = form.value['itemRows'];
      let product_id = null;
      let product_name = null;
      let params = '';
      array.forEach(function (value, index) {
        if ( value.item1.id !== undefined) {
          product_id = value.item1.id;
          product_name = null;
        } else {
          product_id = null;
          product_name = value.item1;
        }
        params += 'name[' + index + ']=' + product_id + '&product_name[' + index + ']=' + product_name + '&quantity[' + index + ']=1'
          + '&unit_price[' + index + ']=' + value.item4 + '&';
      });
      params += 'order_id=' + this.orderIdInv  ;
      this.invoiceService.createInvoice(params).subscribe(
        (data: any[]) => {
          if (data['success'] === true) {
            this.typeSuccess(data['message']);
            this.loginGifSave = false;
            this.modalRef.hide();
            // this.completeOrderComponent.getData();
            this.childEvent.next('true');
          }
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            this.typeError(err.error.message);
            this.loginGifSave = false
          }
        }
      );
    }
  }
  typeSuccess(message) {
    this.toastr.success( message, 'Success!');
  }
  typeError(message) {
    this.toastr.error(message, 'Oopss!!!');
  }

  getDataByOrder() {
    this.invoiceService.newOrderInvoice(this.orderIdInv).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          this.invoiceDetail = data['data'];
          this.products = data['data']['products'];
          this.statuses = data['data']['hold_statuses'];
          this.state = data['data']['products'];
          this.hayaatDiscount = data['data']['hayaat_discount'];
          this.offers = data['data']['offer'];
          if (data['data']['hold_comment'] !== null) {
            this.show = true;
          }
          if (data['data']['document']['medias'].length !== 0) {
            this.thumbnails = data['data']['document']['medias'];
            this.image_src = environment.baseUrl + '/' + data['data']['document']['medias'][0]['image'];
            this.image_small = environment.baseUrl + '/' + data['data']['document']['medias'][0]['image'];
            this.imagesNull = false;
            this.getCount();
          } else {
            this.image_small = '';
            this.image_src = '';
          }

          // if (this.orderDetails.is_prescription) {
          //   this.doctorPrescription = data['data']['prescription']['items'][0]['item'];
          //   this.doctorDetails = data['data']['prescription']['doctor_details'];
          // } else {
          //   this.doctorDetails = [];
          // }

          if (data['data']['promocodes'].length < 1) {
            this.noPromoCode = true;
          } else {
            this.promocodes = data['data']['promocodes'];
          }
          if (this.offers && this.offers.length < 1) {
            this.noOffer = true;
          }
          this.imgLoader = false;
        }
      }
    );
  }

  selectedId(id, index) {
    const unit = this.state.find(x => x.id === id.item.id);
    if (!this.productArray.find(x => x.id === id.item.id)) {
      this.productArray.push(unit);
      this.prices[index] = unit['price'];
      this.calPrice(index);
    } else {
      this.deleteRow(index);
      this.typeError('You have already added this test');
    }
  }
  calPrice(index = null) {
    this.percentages = 0;
    let total: any  = 0;
    let percentageCalculation: any  = 0;
    let discountItemPrice: any  = 0;

    this.prices.forEach(function (value) {
      total = total + parseInt(value, 10);
    });
    this.subTotal = total;

    /*Check if  promocodes are there.*/
    if ( !this.noPromoCode ) {
      this.promocodes.forEach(function (value) {
        this.percentages = this.percentages + parseInt(value.value, 10);
      }, this);
    }
    this.discount[index]  = ( ( ( (this.percentages) / 100 ) * (this.prices[index]) ).toFixed(2) );
    this.discount[index] = 'Rs.'+ this.discount[index]

    /*Check if  offers are there.*/
    if ( !this.noOffer ) {
      this.offers.forEach( function (value) {
        this.percentages = this.percentages + parseInt(value.value, 10);
      }, this);
    }

    percentageCalculation = ( ( ( (this.percentages) / 100 ) * (total) ).toFixed(2) );
    this.total_price = (total - percentageCalculation).toFixed(2);
    if ( this.total_price >= 1000 ) {
      this.orderExceed = true;
    } else {
      this.orderExceed = false
    }
  }
  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => term === '' ? []
        : this.state.filter(v => (v.name + v.code).toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));

  formatter = (x: {name: string}) => x.name;

  onSelectReason(id) {
    if (id === '5' ) {
      this.show = true;
    } else {
      this.show = false;
    }
  }
  savePendingComment(index, comment) {
    if (confirm('Are you sure to make it Pending')) {
      this.loginGif = true;
      const params = 'hold_reason=' + index + '&hold_comment=' + comment;
      this.invoiceService.saveToPending(params, this.orderIdInv).subscribe(
        (data: any[]) => {
          if (data['success'] === true) {
            this.typeSuccess(data['message']);
            this.modalRef.hide();
            this.loginGif = false;
            this.pendingOrderComponent.getData();
            this.childEvent.next('true');
          }
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            this.typeError(err.error.message)
            this.loginGif = false;
          }
        }
      );
    }
  }
  savePending(index) {
    if (confirm('Are you sure to make it Pending')) {
      this.loginGif = true;
      const params = 'hold_reason=' + index;
      this.invoiceService.saveToPending(params, this.orderIdInv).subscribe(
        (data: any[]) => {
          if (data['success'] === true) {
            this.typeSuccess(data['message']);
            this.modalRef.hide();
            this.pendingOrderComponent.getData();
            this.childEvent.next('true');
          }
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            this.typeError(err.error.message);
            this.loginGif = false;
          }
        }
      );
    }
  }
  cancelOrder(id) {
    if (confirm('Are you sure to cancel order')) {
      let params;
      if (id === '1') {
        this.loginGifImageNot = true;
        params = 'status=13'
      } else {
        this.loginGifOrderNot = true;
        params = 'status=12'
      }
      this.invoiceService.cancelPharmacyOrder(params, this.orderIdInv).subscribe(
        (data: any[]) => {
          if (data['success'] === true) {
            this.typeSuccess(data['message']);
            this.modalRef.hide();
            if (!this.flagPending) {
              this.cancelOrderComponent.getData();
            }
            if (this.flagPending) {
              this.cancelOrderComponentPending.getData();
            }
            this.childEvent.next('true');
            this.loginGifImageNot = false;
            this.loginGifOrderNot = false;
          }
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            this.typeError(err.error.message);
            this.loginGif = false;
            this.loginGifImageNot = false;
            this.loginGifOrderNot = false;
          }
        }
      );
    }
  }
}
