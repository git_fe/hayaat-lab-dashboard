import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {AllCustomersService} from '../services/all-customers.service';
import {ToastsManager} from "ng2-toastr";
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  form: FormGroup;
  loginGif = false;
  constructor( private fb: FormBuilder,
               private customerService: AllCustomersService,
               private toastr: ToastsManager,
               private router: Router) { }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.form = this.fb.group({
      newPass: ['', Validators.required],
      confirmPass: ['', Validators.required],
    });
  }
  updatePass(formData) {
    this.loginGif = true;
    let params;
    if (formData.newPass == '' || formData.confirmPass == '') {
      this.passError('Please fill required fields');
      this.loginGif = false;
    } else {
      if (formData.newPass !== formData.confirmPass) {
        this.passError('Passwords Not Match');
        this.loginGif = false;
      } else {
        params = 'is_first_login=1' + '&password=' + formData.newPass + '&password_confirmation=' + formData.confirmPass;
        this.customerService.updatePassword(params).subscribe(
          (data: any[]) => {
            if (data['success'] === true) {
              this.passSuccess(data['message']);
              this.loginGif = false;
              this.router.navigate(['/full-layout']);
            }
          },
          (err) => {
            if (err instanceof HttpErrorResponse) {
              this.passError(err.error.message);
              this.loginGif = false;
            }
          }
        );
      }
    }
  }
  passSuccess(message) {
    this.toastr.success( message, 'Done!');
  }
  passError(message) {
    this.toastr.error(message, 'Oopss!!!');
  }

}

