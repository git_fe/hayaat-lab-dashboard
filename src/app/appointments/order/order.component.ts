import { Component, OnInit, ViewChild , Input  } from '@angular/core';
import { HomeDataService } from "../services/home-data.service";
import { AllOrdersService } from "../services/all-orders.service";
import {CookieService} from "ngx-cookie-service";


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  @Input() pendingOrder;
  @Input() completeOrder;
  @Input() cancelOrder;
  private status = 0;
  public orderId;
  public ordersData: any = [];
  public branches: any = [];
  branchAdmin = false;
  noOrdersData = false;
  imgLoader = true;

  constructor(private newOrder: HomeDataService, public ordersService: AllOrdersService, private cookieService: CookieService) {
  }


  ngOnInit() {
    this.getData();
    if (this.cookieService.get('role') === '2') {
      this.branchAdmin = true;
    }
  }
  getDataCall(call) {
    if (call) {
      this.getData();
    }
  }
  getData() {
    this.newOrder.homeData().subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data']['new_orders'].length !== 0) {
            this.ordersData = data['data']['new_orders'];
            this.noOrdersData = false;
            this.imgLoader = false;
          } else {
            this.noOrdersData = true;
            this.imgLoader = false;
          }
          this.branches = data['data']['branches'];
        }
      }
    );
  }
  getOrderId(id) {
      this.orderId = id;
  }
  newOrdersOfBranch(branchId) {
    this.imgLoader = true;
    this.noOrdersData = false;
    this.newOrder.branchOrders(branchId, this.status).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data'].length !== 0) {
            this.ordersData = data['data'];
            this.noOrdersData = false;
            this.imgLoader = false;
          } else {
            this.noOrdersData = true;
            this.imgLoader = false;
          }
        }
      }
    );
  }
}
