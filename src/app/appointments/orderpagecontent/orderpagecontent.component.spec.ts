import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderpagecontentComponent } from './orderpagecontent.component';

describe('OrderpagecontentComponent', () => {
  let component: OrderpagecontentComponent;
  let fixture: ComponentFixture<OrderpagecontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderpagecontentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderpagecontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
