import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { AllCustomersService } from "../services/all-customers.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { environment } from '../../../environments/environment';
import {ToastsManager} from "ng2-toastr";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent implements OnInit {
  // @ViewChild('form') form_fields;
  // @ViewChild('fileInput') fileInput;
  // formData;
  imagePath = environment.baseUrl;
  form: FormGroup;
  userProfileData: any = [];
  profilePic = false;
  constructor(private user: AllCustomersService,  private fb: FormBuilder, private toastr: ToastsManager) {
  }

  ngOnInit() {
    this.createForm();
    this.getData();
  }
  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      image: ['', Validators],
      avatar: null
    });
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      const files = event.target.files;
      this.form.get('avatar').setValue(files);
    }
  }
  getData() {
    this.user.userProfile().subscribe(
      (data: any[]) => {
        console.log(data['data']);
        this.userProfileData = data['data']['profile_data'];
        if (data['data']['profile_data']['avatar'] === null) {
          this.profilePic = true;
        }
      }
    );
  }
  // upload() {
  //   const fileBrowser = this.fileInput.nativeElement;
  //   console.log(fileBrowser.files[0]);
  //   if (fileBrowser.files && fileBrowser.files[0]) {
  //     this.formData = new FormData();
  //     this.formData.append('image', fileBrowser.files[0]);
  //     console.log('i m here');
  //     console.log(this.formData);
  //   }
  // }
  updateProfileForm(formData) {
    console.log(formData);
    const input = new FormData();
    input.append('name', formData.name);
    input.append('mobile', formData.phone);
    input.append('address', formData.address);
    input.append('email', formData.email);
    if (formData.avatar !== null) {
      const file: File = formData.avatar[0];
      input.append('prescription', file);
    }
    const options = input;
    this.user.updateUserProfile(options).subscribe(
      (data: any[]) => {
        if(data['success'] === true) {
          this.typeSuccess(data['message']);
        }
      },
    (err) => {
      if (err instanceof HttpErrorResponse) {
        this.typeError(err.error.message);
      }
    }
    );
  }
  typeSuccess(message) {
    this.toastr.success( message, 'Success!');
  }
  typeError(message) {
    this.toastr.error(message, 'Oopss!!!');
  }
}
