import { Component, OnInit } from '@angular/core';
import { OffersService } from "../services/offers.service";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-offer-card',
  templateUrl: './offer-card.component.html',
  styleUrls: ['./offer-card.component.scss']
})
export class OfferCardComponent implements OnInit {
  branches = [];
  offers = [];
  pharmacyAdmin = false;
  noOfferFound = false;

  constructor(private offerService: OffersService, private cookieService: CookieService) {
  }

  ngOnInit() {
    this.getOffers();
    if (this.cookieService.get('role') === '2') {
      this.pharmacyAdmin = true;
    }
  }

  getOfferId(id) {
    this.offerService.saveOfferId(id);
  }
  getOffers() {
    this.offerService.getOfferData().subscribe(
      (data: any[]) => {
        if (data['data']['offers'].length !== 0) {
          this.offers = data['data']['offers'];
          this.noOfferFound = false;
        } else {
          this.noOfferFound = true;
        }
        this.branches = data['data']['branches'];
        this.offers.forEach(function (value, index) {
          if (value.type === 1) {
            value.type = '%';
          } else {
            value.type = 'Rs';
          }
        });
      }
    );
  }

  branchOffers(id) {
    this.offerService.getBranchOffer(id).subscribe(
      (data: any[]) => {
        this.offers = data['data']['offers'];
        this.offers.forEach(function (value, index) {
          if (value.type === 1) {
            value.type = '%';
          } else {
            value.type = 'Rs';
          }
        });
      }
    );
  }

  deleteOffer(i, id) {
    const conf = confirm('Are you sure?')
    for (let a = 0; a < this.offers.length; a++) {
      if (a === i) {
        if ( conf === true) {
          const params = '_method=delete';
          this.offerService.deleteOffer(params, id ).subscribe(
            (data: any[]) => {
              console.log(data);
            }
          );
          this.offers.splice(a, 1);
        }
      }
    }
  }
}
