import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

// import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTablesRoutingModule } from "./data-tables-routing.module";
import { ApprovedlistComponent } from './approvedlist/approvedlist.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { HttpClientModule } from "@angular/common/http";
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { OrderpagecontentComponent } from "./orderpagecontent/orderpagecontent.component";
import { GenerateinvoiceComponent} from "./generateinvoice/generateinvoice.component";
import { CustomersComponent } from "./customers/customers.component";
import { CustomercardComponent} from "./customercard/customercard.component";
import { CustomerdetailComponent } from "./customerdetail/customerdetail.component";
import { InvoiceComponent } from "./invoice/invoice.component";
import { InvoicedetailComponent} from "./invoicedetail/invoicedetail.component";
import {InvoicelistComponent} from "./invoicelist/invoicelist.component";
import { BranchesComponent } from './branches/branches.component';
import { BranchcardComponent } from './branchcard/branchcard.component';
import {  BranchdetailComponent} from "./branchdetail/branchdetail.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PharmacybranchService } from "./pharmacybranch.service";
import { AllOrdersService} from "./services/all-orders.service";
import { AllCustomersService } from "./services/all-customers.service";
import { InvoicesService } from "./services/invoices.service";
import { PharmacyBranchesService } from "./services/pharmacy-branches.service";
import { CountdownModule } from 'ngx-countdown';
import { OrdersHistoryComponent } from "./orders-history/orders-history.component";
import { LoginService } from "./services/login.service";
import {SettingsComponent} from "./settings/settings.component";
import {ImageZoomModule} from "angular2-image-zoom";
import {UpdatePharmacyComponent} from "./update-pharmacy/update-pharmacy.component";
import {OffersComponent} from "./offers/offers.component";
import {CreateOfferComponent} from "./create-offer/create-offer.component";
import {UpdateOfferComponent} from "./update-offer/update-offer.component";
import {OfferCardComponent} from "./offer-card/offer-card.component";
import {OffersService} from "./services/offers.service";
import {TruncateModule} from "ng2-truncate";
import {CancelOrdersHistoryComponent} from "./cancel-orders-history/cancel-orders-history.component";
import {ViewPrescriptionComponent} from "./view-prescription/view-prescription.component";
import { ProductsComponent } from './products/products.component';
import {ProductsService} from './services/products.service';
import { UpdateProductsComponent } from './update-products/update-products.component';
import { ReportsComponent } from './reports/reports.component';
import {ChangePasswordComponent} from './change-password/change-password.component';





import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
  MatFormFieldModule,
} from '@angular/material';









@NgModule({
  exports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatFormFieldModule,
  ],
  declarations: [],
})
export class MaterialModule {}





@NgModule({
  imports: [
    // NgxDatatableModule,
    CommonModule,
    DataTablesRoutingModule,
    TabsModule,
    NgbTabsetModule,
    NgbModule,
    ModalModule.forRoot(),
    HttpClientModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    CountdownModule,
    ImageZoomModule,
    TruncateModule,
    MaterialModule
  ],
  declarations: [
    ApprovedlistComponent,
    OrderpagecontentComponent,
    GenerateinvoiceComponent,
    CustomersComponent,
    CustomercardComponent,
    CustomerdetailComponent,
    InvoiceComponent,
    InvoicedetailComponent,
    InvoicelistComponent,
    BranchesComponent,
    BranchcardComponent,
    BranchdetailComponent,
    OrdersHistoryComponent,
    SettingsComponent,
    UpdatePharmacyComponent,
    OffersComponent,
    CreateOfferComponent,
    UpdateOfferComponent,
    OfferCardComponent,
    CancelOrdersHistoryComponent,
    ViewPrescriptionComponent,
    ProductsComponent,
    UpdateProductsComponent,
    ReportsComponent,
    ChangePasswordComponent

  ],
  providers: [
    PharmacybranchService,
    AllOrdersService,
    InvoicesService,
    PharmacyBranchesService,
    LoginService,
    OffersService,
    ProductsService
  ],
  exports: [InvoicedetailComponent]
})
export class DataTablesModule {}
