import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AllCustomersService } from '../services/all-customers.service';

@Component({
  selector: 'app-customercard',
  templateUrl: './customercard.component.html',
  styleUrls: ['./customercard.component.scss']
})
export class CustomercardComponent implements OnInit {
  customersData= [];
  noDataFound = false;
  constructor(private customers: AllCustomersService) { }

  ngOnInit() {
    this.getData();
  }
  getData(): void {
    this.customers.customersData().subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data']['customers'].length !== 0) {
            this.customersData = data['data']['customers'];
            this.noDataFound = false;
          } else {
            this.noDataFound = true;
          }
        }
      }
    );
  }
  customerIndex(customerId) {
    this.customers.customerIndex(customerId);
  }
}

