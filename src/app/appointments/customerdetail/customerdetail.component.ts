import { Component, OnInit } from '@angular/core';
import { AllCustomersService } from "../services/all-customers.service";
import { InvoicesService } from "../services/invoices.service";
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-customerdetail',
  templateUrl: './customerdetail.component.html',
  styleUrls: ['./customerdetail.component.scss']
})
export class CustomerdetailComponent implements OnInit {
  imagePath = environment.baseUrl;
   customerDetail: any = [];
   invoiceRecord = [];


  constructor(private customerRecord: AllCustomersService, private customerInvoice: InvoicesService) {}

  ngOnInit() {
    this.getData();
  }
  getData(): void {
    this.customerRecord.customersDetail().subscribe(
      (data: any[]) => {
         this.customerDetail = data['data']['customer'];
         this.invoiceRecord = data['data']['customer']['orders'];
      }
    );
  }
  getInvoiceIndex(invoiceIndex) {
    this.customerInvoice.customerDetailInvoiceIndex(invoiceIndex);
  }
}
