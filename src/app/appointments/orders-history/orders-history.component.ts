import { Component, OnInit } from '@angular/core';
import { HomeDataService } from "../services/home-data.service";
import {InvoicesService } from "../services/invoices.service";
import {CookieService} from "ngx-cookie-service";
import { environment } from '../../../environments/environment';
import { AllOrdersService } from "../services/all-orders.service";


@Component({
  selector: 'app-orders-history',
  templateUrl: './orders-history.component.html',
  styleUrls: ['./orders-history.component.scss']
})
export class OrdersHistoryComponent implements OnInit {
  private status = 2;
  public completeOrdersData: any = [];
  public branches: any = [];
  branchAdmin = false;
  noCompleteData = false;
  imgLoader = true;
  ordersHistoryData = [];
  orderId;
  noOrdersData = false;
  imagePath = environment.baseUrl;
  pagination = [];
  count;
  page = 1;
  constructor(private orderHistory: AllOrdersService, private invoiceService: InvoicesService) { }
  ngOnInit() {
    this.getData();
  }

  getOrderId(id) {
    this.invoiceService.saveOrderHistoryIndex(id);
  }
  getData() {
    const url = environment.apiUrl + '/orders?history=1';
    this.orderHistory.ordersHistory(url).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data']['orders'].length !== 0) {
            this.ordersHistoryData = data['data']['orders'];
            this.pagination = data['data']['pagination'];
            this.count = data['data']['pagination']['last_page'] + '0';
            this.noOrdersData = false;
            this.imgLoader = false;
            this.noCompleteData = false;
          } else {
            this.noOrdersData = true;
            this.imgLoader = true;
            this.noCompleteData = true;
          }
        }
      });
  }
    pageChanged(event) {
      this.ordersHistoryData = [];
      const nextUrl = environment.apiUrl + '/orders?history=1' + '&page=' + event;
      this.orderHistory.ordersHistory(nextUrl).subscribe(
        (data) => {
          if (data['success'] === true) {
            this.ordersHistoryData = data['data']['orders'];
            this.pagination = data['data']['pagination'];
            this.count = data['data']['pagination']['last_page'] + '0';
          }
        });
    }
}
