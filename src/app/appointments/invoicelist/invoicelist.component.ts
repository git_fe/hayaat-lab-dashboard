import { Component, OnInit } from '@angular/core';
import { InvoicesService } from "../services/invoices.service";
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-invoicelist',
  templateUrl: './invoicelist.component.html',
  styleUrls: ['./invoicelist.component.scss']
})
export class InvoicelistComponent implements OnInit {
  imagePath = environment.baseUrl;
  noInvoiceFound = false;
  invoicesData = [];
  page = 1;
  count;
  pagination = [];
  constructor(private invoice: InvoicesService) { }

  ngOnInit() {
    this.getData();
  }
  getData(): void {
    const url = environment.apiUrl + '/invoices';
    this.invoice.allInvoices(url).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data']['invoices'].length !== 0) {
            this.invoicesData = data['data']['invoices'];
            this.pagination = data['data']['pagination'];
            this.count = data['data']['pagination']['last_page'] + '0';
            this.noInvoiceFound = false;
          } else {
            this.noInvoiceFound = true;
          }
        }
      }
    );
  }
  pageChanged(event) {
    this.invoicesData = [];
    const nextUrl = environment.apiUrl + '/invoices' + '?page=' + event;
    this.invoice.allInvoices(nextUrl).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          this.invoicesData = data['data']['invoices'];
          this.pagination = data['data']['pagination'];
          this.count = data['data']['pagination']['last_page'] + '0';
          this.noInvoiceFound = false;
        } else {
          this.noInvoiceFound = true;
        }
      });
  }
  rowIndex(invoice_i) {
    this.invoice.invoiceDetailIndex(invoice_i);
  }
}


