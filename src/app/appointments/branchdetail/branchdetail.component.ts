import {Component, OnInit, ViewEncapsulation, TemplateRef, enableProdMode} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Http } from '@angular/http';
import { PharmacyBranchesService } from "../services/pharmacy-branches.service";
import { ToastsManager } from "ng2-toastr";


@Component({
  selector: 'app-branchdetail',
  templateUrl: './branchdetail.component.html',
  styleUrls: ['./branchdetail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BranchdetailComponent implements OnInit {
  pharmacyData= [];
  branch= [];
 // model popup code
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService, private http: Http, private createBranch: PharmacyBranchesService, public toastr: ToastsManager) { }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
 // toastr functions
  typeSuccess() {
    this.toastr.success( this.pharmacyData['message'], 'Success!');
  }
  typeError() {
    this.toastr.error(this.pharmacyData['message'], 'Oopss!!!');
  }
  ngOnInit() {
  }
  // function on creating json object on form submit
  addPharmacyBranch(form) {
    const params = 'name=' + form.name + '&email=' + form.email + '&code=' + form.code + '&phone=' + form.phone
      + '&address=' + form.address + '&fax=' + form.fax + '&manager_name=' + form.admin_name
      + '&manager_phone=' + form.admin_phone + '&manager_email=' + form.admin_email;
    this.createBranch.createBranch(params).subscribe(
      (data: any[]) => {
        this.pharmacyData = data;
        if (this.pharmacyData['success'] === true) {
          this.typeSuccess();
        } else if (this.pharmacyData['success'] === false) {
          this.typeError();
        }
      }
    );
  }
}
