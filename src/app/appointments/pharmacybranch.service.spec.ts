import { TestBed, inject } from '@angular/core/testing';

import { PharmacybranchService } from './pharmacybranch.service';

describe('PharmacybranchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PharmacybranchService]
    });
  });

  it('should be created', inject([PharmacybranchService], (service: PharmacybranchService) => {
    expect(service).toBeTruthy();
  }));
});
