import { Component, OnInit, Input } from '@angular/core';
import { HomeDataService } from "../services/home-data.service";
import {CookieService} from "ngx-cookie-service";


@Component({
  selector: 'app-pending-order',
  templateUrl: './pending-order.component.html',
  styleUrls: ['./pending-order.component.scss']
})
export class PendingOrderComponent implements OnInit {
  @Input() cancelOrder;
  private status = 1;
  public pendingFlag = true;
  public orderId;
  public pendingOrderData: any = [];
  public branches: any = [];
  branchAdmin = false;
  noPendingData = false;
  refreshVal = false;
  imgLoader = true;
  constructor(private pendingOrder: HomeDataService, private cookieService: CookieService) {
    // this.pendingOrder.listen().subscribe((m: any) => {
    //   if (m == 'pending') {
    //     this.getData();
    //   }
    // });
  }

  ngOnInit() {
    this.getData();
    if (this.cookieService.get('role') === '2') {
      this.branchAdmin = true;
    }
  }
  getDataCall(event) {
    if (event) {
      this.getData();
    }
  }
  getData() {
    this.pendingOrder.homeData().subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data']['pending_orders'].length !== 0) {
            this.pendingOrderData = data['data']['pending_orders'];
            this.noPendingData = false;
            this.imgLoader = false;
          } else {
            this.noPendingData = true;
            this.imgLoader = false;
          }
          this.branches = data['data']['branches'];
        }
      }
    );
  }
  getOrderId(id) {
    this.orderId = id;
  }
  pendingOrdersOfBranch(branchId) {
    this.imgLoader = true;
    this.noPendingData = false;
    this.pendingOrder.branchOrders(branchId, this.status).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data'].length !== 0) {
            this.pendingOrderData = data['data'];
            this.noPendingData = false;
            this.imgLoader = false;
          } else {
            this.noPendingData = true;
            this.imgLoader = false;
          }
        }
      }
    );
  }

}
