import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { LoginService } from './login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class AllOrdersService {
  public newOrderId;
  constructor(private http: HttpClient, private getHeader: LoginService ) {
  }
  allOrders(url): Observable <any> {
    return this.http.get(url, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  ordersHistory(url): Observable <any> {
    return this.http.get(url, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }

   allOrdersNo(): Observable <any> {
    return this.http.get(environment.apiUrl + '/home/lab-orders', { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }

  orderDetail(id): Observable <any> {
    return this.http.get(environment.apiUrl + '/home/order-details?order_id='+id, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }

  saveFiles(params): Observable <any> {
    return this.http.post(environment.apiUrl + '/home/upload-order-report' , params , { headers: this.getHeader.headers(true)}).map(
      (response) => response);
  }
  ordersCancel(url): Observable <any> {
    return this.http.get(url, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
}
