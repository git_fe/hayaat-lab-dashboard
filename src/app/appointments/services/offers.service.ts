import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { LoginService} from './login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class OffersService {
  offerId;
  constructor(private http: HttpClient, private getHeader: LoginService) { }

  branches(): Observable <any>  {
    return this.http.get(environment.apiUrl + '/branches ', { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  saveOffer(params): Observable <any> {
    return this.http.post(environment.apiUrl + '/offers' , params , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
  getOfferData(): Observable <any> {
    return this.http.get(environment.apiUrl + '/offers'  , { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  getBranchOffer(id): Observable <any>  {
    return this.http.get(environment.apiUrl + '/offers?branch=' + id , { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  saveOfferId(id) {
    this.offerId = id ;
  }
  getOfferById(): Observable <any> {
    return this.http.get(environment.apiUrl + '/offers/' + this.offerId , { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  updateOfferData(params): Observable <any> {
    return this.http.post(environment.apiUrl + '/offers/' + this.offerId , params , { headers: this.getHeader.createPostHeader()}).map(
      (response) => response);
  }
  deleteOffer(params, id): Observable <any> {
    return this.http.post(environment.apiUrl + '/offers/' + id , params , { headers: this.getHeader.createPostHeader()}).map(
      (response) => response);
  }
}
