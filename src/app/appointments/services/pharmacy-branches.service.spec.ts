import { TestBed, inject } from '@angular/core/testing';

import { PharmacyBranchesService } from './pharmacy-branches.service';

describe('PharmacyBranchesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PharmacyBranchesService]
    });
  });

  it('should be created', inject([PharmacyBranchesService], (service: PharmacyBranchesService) => {
    expect(service).toBeTruthy();
  }));
});
