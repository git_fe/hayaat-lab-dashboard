import { Injectable } from '@angular/core';
// import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {CookieService} from "ngx-cookie-service";

@Injectable()
export class LoginService {
  role;
  loginParams = '&grant_type=password&client_id=8&client_secret=mgcdSwLcnTAgz80TrUCIu0yGs3EQaujyWhG6GAPq';
  constructor(private cookieService: CookieService) {
  }
  headers(multipart = false) {
    if (this.cookieService.check('token') ) {
      if ( multipart ) {
        return this.createAuthMultipartHeader();
      } else {
        return this.createAuthHeader();
      }
    } else {
      return this.createHeader();
    }
  }
  createHeader() {
    return new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Accept', 'application/json')
      .append('Authorization', this.cookieService.get('token'));
      // .set('Content-Type', 'application/json')
      // .set('Accept', 'application/json');
  }
  createAuthHeader() {
    return new HttpHeaders()
      .append('Content-Type', 'application/x-www-form-urlencoded')
      .append('Accept', 'application/json')
      .append('Authorization', this.cookieService.get('token'));
  }
  createPostHeader(/*token = null*/) {
    if ( true ) {
      return new HttpHeaders()
        .append('Content-Type', 'application/x-www-form-urlencoded')
        .append('Accept', 'application/json');
        // .append('Authorization', this.cookieService.get('token'));
    }
  }
  createAuthMultipartHeader() {
    // console.log(this.cookieService.check('token'));
    console.log('here');
    if ( true ) {
      return new HttpHeaders()
      // .append('Content-Type', 'multipart/form-data; boundry=')
        .append('Accept', 'application/json')
        .append('Authorization', this.cookieService.get('token'));
    }
  }
}



















  // private baseUrl = 'http://10.5.11.91/';
  // private apiUrl = environment.apiUrl + '/';
  // private accessToken = []; // variable to store the access token
  // // private headers = new HttpHeaders(); // headers for each request
  // // private headers = new HttpHeaders();
  // private header_json = [];
  // // private params = new HttpParams().set('history', '1');
  // // private _options = { headers: new HttpHeaders(
  // //   {'Content-Type': 'application/json'}
  // // )};
  //
  //
  // /* data to get Passport authentication */
  // private postData = {
  //   grant_type: 'password',
  //   client_id: 2,   // the client ID generate before
  //   client_secret: 'BnOxpWZrJf9WHkBahyfygI2eNgpGfBpTibKABnLc',   // the client secret generated before
  //   username: 'jameel@hayaat.pk', // an User in Laravel database
  //   password: 'abc123' // the user's password
  // }
  //
  // constructor(private http: HttpClient) {
  //   // all headers for JSON requests
  //   this.header_json.push('Content-Type', 'application/json');
  //   this.header_json.push('Accept', 'application/json');
  //
  // }
  //
  // loginIt() {
  //   // get authentication token
  //    this.getToken()
  //     .subscribe(data => {
  //       // set headers with Bearer token and save the token to access_token
  //       this.setToken(data['access_token']);
  //     });
  // }
  //
  // getToken() {
  //   return this.http.post(this.baseUrl + 'oauth/token', this.postData, { headers: new HttpHeaders(this.header_json.toString()) })
  // }
  //
  // getHeaders() {
  //   return this.header_json.toString()
  // }
  //
  // setToken(token) {
  //   this.header_json.push('Authorization', 'Bearer ' + token); // add the Authentication header
  //   // console.log(this.headers);
  //   this.accessToken = token;  // save the access_token
  // }

