import { TestBed, inject } from '@angular/core/testing';

import { AllCustomersService } from './all-customers.service';

describe('AllCustomersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllCustomersService]
    });
  });

  it('should be created', inject([AllCustomersService], (service: AllCustomersService) => {
    expect(service).toBeTruthy();
  }));
});
