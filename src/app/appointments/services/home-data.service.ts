import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { LoginService } from './login.service';
import { environment } from '../../../environments/environment';
import {Subject} from "rxjs/Subject";

@Injectable()
export class HomeDataService {
  pharmacyDetail;
  constructor(private http: HttpClient, private getHeader: LoginService ) {}

  loginAdmin(params): Observable <any> {
    return this.http.post(environment.baseUrl + '/oauth/token', params + this.getHeader.loginParams , { headers: this.getHeader.createPostHeader()}).map(
      (response) => response);
  }
  homeData(): Observable <any> {
    return this.http.get(environment.apiUrl + '/dashboard', { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
  branchOrders(branchId, status): Observable <any> {
    // this.branchId = branchId;
    const branchParams = new HttpParams().set('status', status).set('branch', branchId);
    return this.http.get(environment.apiUrl + '/dashboard/orders', { params: branchParams, headers: this.getHeader.headers()}).map(
      (response) => response);
  }

}
