import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { LoginService } from './login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class PharmacyBranchesService {
  private branch_id;
  constructor(private http: HttpClient, private getHeader: LoginService ) {}
  allBranches(): Observable <any> {
    return this.http.get(environment.apiUrl + '/branches', { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  createBranch(params): Observable <any> {
    return this.http.post(environment.apiUrl + '/branches', params , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
  saveBranchId(branchId) {
    this.branch_id = branchId;
  }
  getBranchDetail() {
    return this.http.get(environment.apiUrl + '/branches/' + this.branch_id, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  updateBranch(params): Observable <any> {
    return this.http.post(environment.apiUrl + '/branches/' + this.branch_id, params , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
}
