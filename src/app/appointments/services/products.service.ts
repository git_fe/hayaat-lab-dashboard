import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { LoginService} from './login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class ProductsService {
  productId;
  constructor(private http: HttpClient, private getHeader: LoginService) { }

  saveProduct(params): Observable <any> {
    return this.http.post(environment.apiUrl + '/products' , params , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }

  getCategories(): Observable <any> {
    return this.http.get(environment.apiUrl + '/home/categories-based-type?type=2', { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }

  getProducts(): Observable <any> {
    return this.http.get(environment.apiUrl + '/products/get-products', { headers: this.getHeader.headers()}).map(
      (response) => response);
  }

  deleteProducts(id): Observable <any> {
    return this.http.delete(environment.apiUrl + '/products/' + id, { headers: this.getHeader.headers()}).map(
      (response) => response);
  }

  updateProduct(id, params): Observable <any> {
    return this.http.post(environment.apiUrl + '/products/' + id , params , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }

}
