import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { LoginService} from './login.service';
import { environment } from '../../../environments/environment';
// import {AllOrdersService} from "./all-orders.service";


@Injectable()
export class InvoicesService {
private customerHistoryIndex;
 orderHistoryIndex;
invoice_index;
 completeOrderIndex;
  constructor(private http: HttpClient, private getHeader: LoginService) {}

  // Invoices HTML and Invoice Detail HTML
  allInvoices(url): Observable <any> {
    return this.http.get(url, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  invoiceDetailIndex(invoice_i) {
    this.invoice_index = invoice_i;
  }
  saveOrderHistoryIndex(id) {
    this.orderHistoryIndex = id;
  }
  saveCompleteOrderIndex(id) {
    this.completeOrderIndex = id;
  }
  invoiceDetail(): Observable <any> {

    return this.http.get(environment.apiUrl + '/invoices/' + this.invoice_index, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  // Invoice detail for complete orders view and order history view
  completeOrderInvoiceData(): Observable <any> {
    return this.http.get(environment.apiUrl + '/invoices/' + this.completeOrderIndex, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  orderInvoiceData(): Observable <any> {
    return this.http.get(environment.apiUrl + '/invoices/' + this.orderHistoryIndex, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }

  // For Customer Detail HTML and Customer Detail Invoice HTML
  customerDetailInvoiceIndex(invoiceIndex) {
    this.customerHistoryIndex = invoiceIndex;
  }
  customerInvoiceData(): Observable <any> {
    return this.http.get(environment.apiUrl + '/invoices/' + this.customerHistoryIndex, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  createInvoice(params): Observable <any> {
    return this.http.post(environment.apiUrl + '/invoices', params , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
  // Invoice creation for new order
  newOrderInvoice(id) {
    return this.http.get(environment.apiUrl + '/lab/process-order/' + id , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
  saveToPending(params, orderId) {
    return this.http.post(environment.apiUrl + '/orders/hold/'  + orderId , params , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
  getMedList()  {
    return this.http.get(environment.apiUrl + '/products' , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
  cancelPharmacyOrder(params, id) {
    return this.http.post(environment.apiUrl + '/orders/cancel/' + id , params, { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
  updateOrder(id, params): Observable <any> {
    return this.http.post(environment.apiUrl + '/orders/' + id , params , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
  getTestItems()  {
    return this.http.get(environment.apiUrl + '/home/lab-testlistWithPrice' , { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
}
