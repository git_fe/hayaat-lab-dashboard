import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { LoginService } from './login.service';
import { environment } from '../../../environments/environment';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class AllCustomersService {
  private customerId;
  userProfileDetail;
  _listners = new Subject<any>();
  constructor(private http: HttpClient, private getHeader: LoginService ) {}

  listen(): Observable<any> {
    return this._listners.asObservable();
  }

  checkFlag(filterBy: string) {
    this._listners.next(filterBy);
  }



  customersData(): Observable <any> {
    return this.http.get(environment.apiUrl + '/customers', { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  customerIndex(customerId) {
    this.customerId = customerId;
  }
  customersDetail(): Observable <any> {
    return this.http.get(environment.apiUrl + '/customers/' + this.customerId, { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  userProfile(): Observable <any> {
    return this.http.get(environment.apiUrl + '/dashboard/profile', { headers: this.getHeader.createHeader()}).map(
      (response) => response);
  }
  updateUserProfile(params): Observable <any> {
    return this.http.post(environment.apiUrl + '/dashboard/profile?_method=put', params   , { headers: this.getHeader.headers(true)}).map(
      (response) => response);
  }
  updatePassword(params) {
    return this.http.post(environment.apiUrl + '/password/change?_method=put', params  , { headers: this.getHeader.headers()}).map(
      (response) => response);
  }
}
