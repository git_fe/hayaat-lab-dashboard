import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ApprovedlistComponent} from './approvedlist/approvedlist.component';
import {OrderpagecontentComponent} from "./orderpagecontent/orderpagecontent.component";
import {CustomersComponent} from "./customers/customers.component";
import {CustomerdetailComponent} from "./customerdetail/customerdetail.component";
import {InvoiceComponent} from "./invoice/invoice.component";
import {BranchesComponent} from "./branches/branches.component";
import {SettingsComponent} from "./settings/settings.component";
import {OffersComponent} from "./offers/offers.component";
import {AuthGuard} from "../auth.guard";
import {ProductsComponent} from './products/products.component';
import { ReportsComponent } from './reports/reports.component';
import {ChangePasswordComponent} from './change-password/change-password.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'approvedlist',
        component: ApprovedlistComponent,
        data: {
          title: 'Approved List Data Table'
        }
      }
    ]
  },
  { path: 'orders', canActivate: [AuthGuard] , component: OrderpagecontentComponent},
  { path: 'customer', canActivate: [AuthGuard] , component: CustomersComponent},
  { path: 'customerdetail', canActivate: [AuthGuard]  , component: CustomerdetailComponent},
  { path: 'invoice', canActivate: [AuthGuard]  , component: InvoiceComponent},
  { path: 'branches', canActivate: [AuthGuard] , component: BranchesComponent},
  { path: 'setting', canActivate: [AuthGuard]  , component: SettingsComponent},
  { path: 'offers', canActivate: [AuthGuard] , component: OffersComponent},
  { path: 'products', canActivate: [AuthGuard] , component: ProductsComponent},
  { path: 'reports', canActivate: [AuthGuard] , component: ReportsComponent},
  { path: 'change-password', canActivate: [AuthGuard] , component: ChangePasswordComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataTablesRoutingModule { }
