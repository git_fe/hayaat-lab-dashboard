import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InProcessOrderComponent } from './in-process-order.component';

describe('InProcessOrderComponent', () => {
  let component: InProcessOrderComponent;
  let fixture: ComponentFixture<InProcessOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InProcessOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InProcessOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
