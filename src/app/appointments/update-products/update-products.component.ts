import {Component, Input, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {ToastsManager} from 'ng2-toastr';
import {ProductsService} from '../services/products.service';
import {ProductsComponent} from '../products/products.component';
import {formatDate} from 'ngx-bootstrap/chronos';

@Component({
  selector: 'app-update-products',
  templateUrl: './update-products.component.html',
  styleUrls: ['./update-products.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UpdateProductsComponent implements OnInit {
  @Input()row;
  public model: any;
  products = this.product.products;
  categories = this.product.categories;
  allProducts = this.product.products;
  loadComplete = false;
  noProductFound = false;
  productName = null;
  productId = null;
  productSelected = null;
  categorySelected = null;
  categoryID = null;
  price = null;
  form: FormGroup;
  modalRef: BsModalRef;
  @ViewChild('instance') instance: NgbTypeahead;
  totalRows: any = [];
  focus1$ = new Subject<string>();
  click1$ = new Subject<string>();
  search1 = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => term === '' ? []
        : this.categories.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
  search2 = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => term === '' ? []
        : this.products.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
  formatter = (x: {name: string}) => x.name;
  constructor(private productsService: ProductsService,
              private modalService: BsModalService,
              private fb: FormBuilder, private toastr: ToastsManager,
              private product: ProductsComponent) { }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  ngOnInit() {
    this.createForm();
  }
  assignVal(finder, type, edit) {
    if(edit === 'edit') {
      if (type === 'category') {
        const finded = this.categories.find(x => x.id === finder.category.id);
        this.categorySelected = finded;
      } else if (type === 'product') {
        const finded = this.products.find(x => x.id === finder.id);
        this.productSelected = finded;
      }
    } else if (edit === 'assign') {
      console.log(finder);
      if (type === 'category') {
        const finded = this.categories.find(x => x.id === finder.item.id);
        this.categoryID = finded['id']
      } else if (type === 'product') {
        const finded = this.products.find(x => x.id === finder.item.id);
        this.productId = finded['id'];
      }
    } else if (edit === '') {
      if (type === 'category') {
        const finded = this.categories.find(x => x.id === finder.id);
        this.categoryID = finded['id']
      } else if (type === 'product') {
        const finded = this.products.find(x => x.id === finder.id);
        this.productId = finded['id'];
        this.productName = finded['name'];
      }
    }

  }
  createForm() {
    this.form = this.fb.group({
      name1: ['', Validators.required],
      category1: ['', Validators.required],
      price1: ['', Validators.required]
    });
  }
  editProduct(row) {
    this.assignVal(row, 'category', 'edit');
    this.assignVal(row, 'product', 'edit');
    this.form.setValue({category1: this.categorySelected, name1: this.productSelected, price1 : row.unit_price});
  }
  updateProduct(formData, event: Event) {
    event.preventDefault();
    this.assignVal(formData.category1, 'category', '');
    if (typeof formData.name1 === 'string') {
        this.productId = null;
        this.productName = formData.name1;
    } else {
      this.assignVal(formData.name1, 'product', '');
    }
    const params = 'id=' + this.productId + '&name=' + this.productName + '&category_id=' + this.categoryID + '&unit_price=' + formData.price1 + '&_method=put';
    this.productsService.updateProduct(this.row.id, params).subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          this.modalRef.hide();
          this.product.getProducts();
          this.form.reset();
          this.product.typeSuccess(data['message']);
        }
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          this.product.typeError(err.error.message);
        }
      }
    );
  }

}
