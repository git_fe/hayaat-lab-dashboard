import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {OffersService } from "../services/offers.service";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import {CookieService} from "ngx-cookie-service";
const now = new Date();
@Component({
  selector: 'app-update-offer',
  templateUrl: './update-offer.component.html',
  styleUrls: ['./update-offer.component.scss']
})
export class UpdateOfferComponent implements OnInit {
  units = ['fixed', 'percentage'];
  offerStartDate;
  offerEndDate;
  // Variable declaration
  d: any;
  model: NgbDateStruct;
  popupModel;
  popupModel2;
  pharmacyAdmin = false;
  date: {year: number, month: number};

  configModal;    // Global configuration of datepickers

  offerData = [];
  branches = [];
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService, public offerService: OffersService, private cookieService: CookieService) { }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  // Selects today's date
  selectToday() {
    this.model = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
  }

  ngOnInit() {
    this.selectToday();
    this.getBranches();
    if (this.cookieService.get('role') === '2') {
      this.pharmacyAdmin = true;
    }
  }
  getBranches() {
    this.offerService.branches().subscribe(
      (data: any[]) => {
        this.branches = data['data']['branches'];
      }
    );
  }
  getOffer() {
    this.offerService.getOfferById().subscribe(
      (data: any[]) => {
        console.log(data);
        this.offerData = data['data'];
         if (this.offerData['type'] === 1) {
         this.offerData['type'] = 'percentage';
          } else {
           this.offerData['type'] = 'fixed';
          }
        this.offerStartDate = data['data']['start_date'];
        this.offerEndDate = data['data']['end_date'];
        // console.log(this.popupModel);
        // console.log(this.popupModel2);
      }
    );
  }
  updateOffer(form, branch, unit) {
    console.log(form, unit , branch);
    let params;
    if ((form.startDate = 'undefined') || (form.endDate = 'undefined') ) {
      params = 'branch=' + branch + '&name=' + form.offerName + '&value=' + form.percentage + '&type=' + unit + '&start=' + this.offerStartDate
        + '&end=' + this.offerEndDate + '&_method=put';
    } else {
      const startDate = JSON.stringify(form.startDate);
      const endDate = JSON.stringify(form.endDate);
      params = 'branch=' + branch + '&name=' + form.offerName + '&value=' + form.percentage + '&type=' + unit + '&start=' + startDate
        + '&end=' + endDate + '&_method=put';
    }
      console.log(params);
      this.offerService.updateOfferData(params).subscribe(
        (data: any[]) => {
          console.log(data);
        }
      );
    }
}
