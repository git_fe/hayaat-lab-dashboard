import { RouteInfo } from './sidebar.metadata';

//Sidebar menu Routes and data
export const ROUTES: RouteInfo[] = [

  {
    path: '/full-layout', title: 'Dashboard', icon: 'ft-home', class: '', badge: '', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false, submenu: []
  //   { path: '/full-layout', title: 'Dashboard1', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //   { path: '/dashboard/dashboard2', title: 'Dashboard2', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  // ]
  },


  // {
  //   path: '', title: 'Appointments', icon: 'ft-calendar', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
  //   submenu: [
  //
  //     { path: '/datatables/filter', title: 'Pending List', icon: '', class: '', badge: '50', badgeClass: 'badge badge-pill badge-success float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
  //     { path: '/datatables/approvedlist', title: 'Approved List', icon: '', class: '', badge: '1', badgeClass: 'badge badge-pill badge-success float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/syntaxhighlighter', title: 'Done List', icon: '', class: '', badge: '3', badgeClass: 'badge badge-pill badge-success float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/helperclasses', title: 'Reject List', icon: '', class: '', badge: '10', badgeClass: 'badge badge-pill badge-success float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/textutilities', title: 'Cancel List', icon: '', class: '', badge: '13', badgeClass: 'badge badge-pill badge-success float-right mr-1 mt-1', isExternalLink: false, submenu: [] },
  //
  //
  //
  //   ]
  // },
  // {
  //   path: '', title: 'Doctors', icon: 'ft-aperture', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
  //   submenu: [
  //
  //     { path: '/uikit/grids', title: 'All Doctors', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/typography', title: 'Add Doctors', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/syntaxhighlighter', title: 'Speciality', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/helperclasses', title: 'Doctors Feedback', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/textutilities', title: 'Doctors For Approval', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/textutilities', title: 'Archive Doctors', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //
  //   ]
  // },
  // {
  //   path: '', title: 'Blood Doners', icon: 'ft-aperture', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
  //   submenu: [
  //
  //     { path: '/uikit/grids', title: 'All Doners', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/typography', title: 'Add Doners', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //
  //
  //   ]
  // },
  // {
  //   path: '', title: 'Emergency', icon: 'ft-aperture', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
  //   submenu: [
  //
  //     { path: '/uikit/grids', title: 'All Emergency', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/typography', title: 'Add Emergency', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //
  //
  //   ]
  // },
  // {
  //   path: '', title: 'Frontdesk Users', icon: 'ft-user', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
  //   submenu: [
  //
  //     { path: '/uikit/grids', title: 'All Frontend Users', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/typography', title: 'Add Frontend Users', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //
  //
  //   ]
  // },
  //
  // {
  //   path: '', title: 'App Feedback', icon: 'ft-aperture', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
  //   submenu: [
  //
  //     { path: '/uikit/grids', title: 'All Feednack', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //
  //
  //   ]
  // },
  // {
  //   path: '', title: 'Blogs', icon: 'ft-aperture', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
  //   submenu: [
  //
  //     { path: '/uikit/grids', title: 'All Blogs', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/typography', title: 'Add Blog', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/typography', title: 'Add Blog Tags', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //
  //
  //   ]
  // },
  // {
  //   path: '', title: 'Locations', icon: 'ft-aperture', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
  //   submenu: [
  //
  //     { path: '/uikit/grids', title: 'Cities', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //     { path: '/uikit/typography', title: 'Areas', icon: '', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //
  //
  //   ]
  // },
  {
    path: '/user-login', title: 'Login', icon: 'ft-square', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  },
  { path: '/datatables/orders', title: 'All Orders', icon: 'ft-user', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  { path: '/datatables/customer', title: 'Customer', icon: 'ft-user', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  { path: '/datatables/invoice', title: 'Invoice', icon: 'ft-file-text', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  // { path: '/datatables/setting', title: 'Settings', icon: 'ft-settings', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  // { path: '/datatables/branches', title: 'Branches', icon: 'ft-user', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  // { path: '/datatables/offers', title: 'Offers', icon: 'ft-user', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  // { path: '/datatables/products', title: 'Products', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  { path: '/datatables/reports', title: 'Reports', icon: 'ft-list', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  { path: '/datatables/change-password', title: 'Change Password', icon: 'ft-edit-3', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },



];
