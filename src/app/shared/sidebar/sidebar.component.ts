import { Component, OnInit } from '@angular/core';
import { ROUTES } from './sidebar-routes.config';
import { RouteInfo } from "./sidebar.metadata";
import { Router, ActivatedRoute } from "@angular/router";
import {CookieService} from "ngx-cookie-service";
import {LoginService} from "../../appointments/services/login.service";
import { environment } from '../../../environments/environment';
import {HomeDataService} from "../../appointments/services/home-data.service";
import {AllCustomersService} from '../../appointments/services/all-customers.service';

declare var $: any;
@Component({
    // moduleId: module.id,
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    pharmacyHomeDetail;
    imagePath = environment.baseUrl;
    public menuItems: any[];
    userProfileData:any = [];
    profilePic = false;


    constructor(private router: Router,
        private route: ActivatedRoute, private cookieService: CookieService, private loginService: LoginService,
                private homeService: HomeDataService,
                private customerService: AllCustomersService) {

      this.customerService.listen().subscribe((m: any) => {
        if (m == 'true') {
          this.userProfileData = this.customerService.userProfileDetail;
        }
      });
    }

    ngOnInit() {
        $.getScript('./assets/js/app-sidebar.js');
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        this.pharmacyHomeDetail = this.homeService.pharmacyDetail;
    }
}
