import { Component, OnInit } from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {LoginService} from "../../appointments/services/login.service";
import {AllCustomersService} from '../../appointments/services/all-customers.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent {
  pharmacyLogin = false;
  constructor(private cookieService: CookieService, private router: Router, private loginService: LoginService,
              private customerService: AllCustomersService) {}

  ngOnInit() {
    if (this.cookieService.get('role') === '2') {
      this.pharmacyLogin = true;
    }
  }
  logout() {
    this.cookieService.delete('token');
    this.cookieService.delete('role');
    this.router.navigate(['/user-login']);
  }
}
