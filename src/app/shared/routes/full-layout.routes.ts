import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from "../../auth.guard";

//Route for content layout with sidebar, navbar and footer
export const Full_ROUTES: Routes = [
  {
    path: 'changelog',
    loadChildren: './changelog/changelog.module#ChangeLogModule'
  },
  {
    path: 'full-layout',
    loadChildren: './pages/full-layout-page/full-pages.module#FullPagesModule'
  },
  {
    path: 'datatables',
    loadChildren: './appointments/data-tables.module#DataTablesModule'
  }
];



