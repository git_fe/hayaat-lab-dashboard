import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullLayoutPageComponent } from 'app/pages/full-layout-page/full-layout-page.component';
import {AuthGuard} from "../../auth.guard";

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
     component: FullLayoutPageComponent,
    data: {
      title: 'Full Layout Page'
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullPagesRoutingModule { }
