import { Component, OnInit} from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent} from 'ng-chartist';
// import { Http, Headers, RequestOptions } from "@angular/http";
import { HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import 'rxjs/add/operator/map';
import { HomeDataService } from "../../appointments/services/home-data.service";
import {LoginService} from "../../appointments/services/login.service";
import {CookieService} from "ngx-cookie-service";
import {AllCustomersService} from '../../appointments/services/all-customers.service';

declare var $: any;

// declare var require: any;
//
// const data: any = require('../../pages/chartist.json');
//
//
// export interface Chart {
//   type: ChartType;
//   data: Chartist.IChartistData;
//   options?: any;
//   responsiveOptions?: any;
//   events?: ChartEvent;
// }

@Component({
  selector: 'app-full-layout-page',
  templateUrl: './full-layout-page.component.html',
  styleUrls: ['./full-layout-page.component.scss']
})
export class FullLayoutPageComponent {
  homeData: any = [];
  adminRole = false;
  constructor(private homeservice: HomeDataService, private loginService: LoginService, private cookieService: CookieService,
              private customerService: AllCustomersService) {
  }
   ngOnInit() {
       this.getData();
       if (this.cookieService.get('role') === '2') {
         this.adminRole = true;
         this.navHide();
       }
   }

  getData() {
    this.homeservice.homeData().subscribe(
       (data: any[]) => {
         if (data['success'] === true) {
           this.homeData = data['data'];
           this.customerService.userProfileDetail = data['data']['lab_details'];
           this.customerService.checkFlag('true');
           this.loginService.role = data['data']['role'];
           if (data['data']['role'] === 2) {
             this.cookieService.set( 'role', data['data']['role'], data['expires_in'] );
             this.adminRole = true;
             this.navHide();
           }
         }
       }
     );
   }
  navHide() {
    setTimeout(() => {
      $('ul.navigation li:nth-child(5)').css('display', 'none', 'important');
      $('ul.navigation li:nth-child(6)').css('display', 'none', 'important');
      $('ul.navigation li:nth-child(7)').css('display', 'none', 'important');
    }, 1000);
  }
}
