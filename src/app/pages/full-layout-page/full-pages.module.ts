import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { FullPagesRoutingModule } from "./full-pages-routing.module";

import { FullLayoutPageComponent } from './full-layout-page.component';
import {OrderComponent} from "../../appointments/order/order.component";
// import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PendingOrderComponent } from "../../appointments/pending-order/pending-order.component";
import { CompletedOrderComponent } from "../../appointments/completed-order/completed-order.component";
import {CancelOrderComponent} from "../../appointments/cancel-order/cancel-order.component";
import { InProcessOrderComponent } from "../../appointments/in-process-order/in-process-order.component";
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { HttpClientModule } from "@angular/common/http";
import { LoginService } from "../../appointments/services/login.service";
import { HomeDataService } from "../../appointments/services/home-data.service";
import {AllOrdersService} from "../../appointments/services/all-orders.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {ImageZoomModule} from "angular2-image-zoom";
import {CountdownModule} from "ngx-countdown";
import { InvoicesService } from "../../appointments/services/invoices.service";
import {DataTablesModule} from "../../appointments/data-tables.module";
import {AuthGuard} from "../../auth.guard";



import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
  MatFormFieldModule,
} from '@angular/material';





@NgModule({
  exports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatFormFieldModule,
  ],
  declarations: [],
})
export class MaterialModule {}

@NgModule({
  imports: [
    CommonModule,
    FullPagesRoutingModule,
    // NgxDatatableModule,
    NgbTabsetModule,
    TabsModule,
    NgxChartsModule,
    Ng2SmartTableModule,
    NgbModule.forRoot(),
    ModalModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ImageZoomModule,
    CountdownModule,
    MaterialModule,
    DataTablesModule



  ],
  declarations: [
    FullLayoutPageComponent,
    OrderComponent,
    PendingOrderComponent,
    CompletedOrderComponent,
    CancelOrderComponent,
    InProcessOrderComponent,
    // InvoicedetailComponent
  ],
  providers: [ LoginService, HomeDataService, AllOrdersService, InvoicesService, AuthGuard ]
})
export class FullPagesModule { }
