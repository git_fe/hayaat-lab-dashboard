import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {HomeDataService} from "../../appointments/services/home-data.service";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {ToastsManager} from "ng2-toastr";
import {HttpErrorResponse} from "@angular/common/http";


@Component({
  selector: 'app-content-layout-page',
  templateUrl: './content-layout-page.component.html',
  styleUrls: ['./content-layout-page.component.scss']
})
export class ContentLayoutPageComponent {
  form: FormGroup;
  loginForm = false;
  loginGif = false;
  constructor(
    private fb: FormBuilder,
    private homeService: HomeDataService,
    private cookieService: CookieService,
    private router: Router,
    private toastr: ToastsManager
  ) {}

  ngOnInit() {
    this.createForm();
    if (location.pathname == '/user-login' && this.cookieService.check('token')) {
      this.router.navigate(['/full-layout']);
      this.loginForm = true;
    }
  }
  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  typeError(message) {
    this.toastr.error(message, 'Oopss!!!');
  }
  typeSuccess() {
    this.toastr.success('Login Successfully', 'Welcome');
  }
  userLogin(form) {
    this.loginGif = true;
    if ( form.name == '' || form.password == '') {
      this.typeError('Enter Both Fields');
      this.loginGif = false;
    } else {
      const params = 'username=' + form.name + '&password=' + form.password;
      this.homeService.loginAdmin(params).subscribe(
        (data: any[]) => {
          if (data['access_token'] !== '') {
            this.typeSuccess();
            this.cookieService.set('token', 'Bearer ' + data['access_token'], data['expires_in']);
            this.getData();
          }
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            this.loginGif = false;
            this.typeError(err.error.message);
          }
        }
      );
    }
  }
  getData() {
    this.homeService.homeData().subscribe(
      (data: any[]) => {
        if (data['success'] === true) {
          if (data['data']['role'] === 2) {
            this.cookieService.set( 'role', data['data']['role'], data['expires_in'] );
            this.loginGif = false;
          }
          this.router.navigate(['/full-layout']);
        }
      }
    );
  }
}
