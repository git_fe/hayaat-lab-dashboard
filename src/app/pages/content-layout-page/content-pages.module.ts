import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from "@angular/forms";
import {HomeDataService} from "../../appointments/services/home-data.service";
import { HttpClientModule } from "@angular/common/http";
import {LoginService} from "../../appointments/services/login.service";
import {CookieService} from "ngx-cookie-service";

import { ContentPagesRoutingModule } from "./content-pages-routing.module";
import { ContentLayoutPageComponent } from './content-layout-page.component';



@NgModule({
    imports: [
        CommonModule,
        ContentPagesRoutingModule,
        FormsModule,
      ReactiveFormsModule,
      HttpClientModule
    ],
    declarations: [
        ContentLayoutPageComponent
    ],
  providers: [ HomeDataService, LoginService, CookieService]
})
export class ContentPagesModule { }
