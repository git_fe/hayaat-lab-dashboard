
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from "./shared/shared.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { AppComponent } from './app.component';
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";
import { ChartistModule} from 'ng-chartist';
import {ToastModule, ToastOptions} from "ng2-toastr";
import {DatePipe} from "@angular/common";
import {ImageZoomModule } from "angular2-image-zoom";
import {ReactiveFormsModule} from "@angular/forms";
import {AuthGuard} from "./auth.guard";
import {LoginService} from "./appointments/services/login.service";
import {CookieService} from "ngx-cookie-service";
import {HomeDataService} from "./appointments/services/home-data.service";
import { HttpModule } from '@angular/http';
import {HttpClientModule} from "@angular/common/http";
import {AllCustomersService} from './appointments/services/all-customers.service';







// import {ChartistComponent} from 'ng-chartist';



// import { ChangeLogComponent } from './changelog/changelog.component';
// import { FullLayoutPageComponent } from './pages/full-layout-page/full-layout-page.component';
// import { ContentLayoutPageComponent } from './pages/content-layout-page/content-layout-page.component';

// import * as $ from 'jquery';





@NgModule({
    declarations: [
        AppComponent,
        FullLayoutComponent,
        ContentLayoutComponent,










        // ChartistComponent
        // ChangeLogComponent,
        // FullLayoutPageComponent,
        // ContentLayoutPageComponent
    ],
    imports: [
        BrowserAnimationsModule,
        AppRoutingModule,
        SharedModule,
        NgbModule.forRoot(),
        // NgxDatatableModule,
        ChartistModule,
        ToastModule.forRoot(),
        ImageZoomModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpModule
    ],
    providers: [
      ToastOptions,
      DatePipe,
      AuthGuard,
      LoginService,
      CookieService,
      HomeDataService,
      AllCustomersService

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
